package com.mowbs.trivia.config;

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class HostIsLocalFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		
		String remoteIp = request.getRemoteAddr();
		String localIp = InetAddress.getLocalHost().getHostAddress();
		String knownLocal = "0:0:0:0:0:0:0:1";

		boolean isLocal = remoteIp.equals(localIp) || remoteIp.equals(knownLocal);
		
		if (request.getServletPath().trim().toLowerCase().contains("host") && !isLocal) {
			response.sendError(HttpStatus.FORBIDDEN.value());
		} else {
			chain.doFilter(servletRequest, servletResponse);
		}		
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
	

}
