package com.mowbs.trivia.domain;

public class AdjustVolumeRequest {
	
	private AudioVolume volume;
	
	public AudioVolume getVolume() {
		return volume;
	}
	
	public void setVolume(AudioVolume volume) {
		this.volume = volume;
	}

}
