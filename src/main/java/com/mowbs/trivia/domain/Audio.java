package com.mowbs.trivia.domain;

public interface Audio {
	
	public String getFileName();

}
