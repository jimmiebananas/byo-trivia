package com.mowbs.trivia.domain;

public class AudioState {
	
	private AudioVolume musicVolume;
	private AudioVolume sfxVolume;
	
	public AudioState(AudioVolume musicVolume, AudioVolume sfxVolume) {
		this.musicVolume = musicVolume;
		this.sfxVolume = sfxVolume;
	}
	
	public AudioVolume getMusicVolume() {
		return musicVolume;
	}
	
	public void setMusicVolume(AudioVolume musicVolume) {
		this.musicVolume = musicVolume;
	}
	
	public AudioVolume getSfxVolume() {
		return sfxVolume;
	}
	
	public void setSfxVolume(AudioVolume sfxVolume) {
		this.sfxVolume = sfxVolume;
	}
}
