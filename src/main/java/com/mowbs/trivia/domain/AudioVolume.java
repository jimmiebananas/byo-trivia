package com.mowbs.trivia.domain;

public enum AudioVolume {
	
	ONE(-24f),
	TWO(-21f),
	THREE(-18f),
	FOUR(-15f),
	FIVE(-12f),
	SIX(-9f),
	SEVEN(-6f),
	EIGHT(-3f),
	NINE(0f),
	TEN(3f),
	MUTE(-80f);
	
	AudioVolume(float decibalValue) {
		this.decibalValue = decibalValue;
	}
	
	private float decibalValue;
	
	public float getDecibalValue() {
		return decibalValue;
	}

}
