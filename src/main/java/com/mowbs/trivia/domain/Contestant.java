package com.mowbs.trivia.domain;

public final class Contestant {

	private String name;
	private String id;
	private int score = 0;
	private boolean answeringEnabled = true;
	private PlayerSound buzzerSound;
	
	public Contestant() {
		
	}
	
	public Contestant(String name, String id, PlayerSound buzzerSound) {
		this.name = name;
		this.id = id;
		this.buzzerSound = buzzerSound;
	}
	
	public Contestant(String name, String id, int score) {
		this.name = name;
		this.id = id;
		this.score = score;
	}
	
	public void incrementScore(int incrementValue) {
		score += incrementValue;
	}
	
	public void setAnsweringEnabled(boolean answeringEnabled) {
		this.answeringEnabled = answeringEnabled;
	}
	
	public String getName() {
		return name;
	}
	
	public String getId() {
		return id;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public PlayerSound getBuzzerSound() {
		return buzzerSound;
	}
	
	public boolean isAnsweringEnabled() {
		return answeringEnabled;
	}
}
