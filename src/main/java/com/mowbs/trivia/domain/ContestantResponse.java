package com.mowbs.trivia.domain;

public class ContestantResponse implements Comparable<ContestantResponse> {
	private String name;
	private String id;
	private int score = 0;
	private boolean answering;
	private boolean answeringEnabled;
	
	public ContestantResponse(Contestant contestant, boolean isAnswering) {
		this.name = contestant.getName();
		this.id = contestant.getId();
		this.score = contestant.getScore();
		this.answering = isAnswering;
		this.answeringEnabled = contestant.isAnsweringEnabled();
	}
	
	public boolean isAnswering() {
		return answering;
	}
	
	public boolean isAnsweringEnabled() {
		return answeringEnabled;
	}
	
	public String getName() {
		return name;
	}
	
	public String getId() {
		return id;
	}
	
	public int getScore() {
		return score;
	}

	@Override
	public int compareTo(ContestantResponse other) {
		return other.getName().compareTo(name);
	}

}
