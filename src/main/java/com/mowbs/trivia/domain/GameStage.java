package com.mowbs.trivia.domain;

public enum GameStage {
	NOT_CREATED(false, false),
	CREATED(false, true),
	STARTED(false, false),
	DISPLAYING_ANSWER(false, false),
	DISPLAYING_TRANSITION(false, false),
	DISPLAYING_QUESTION(true, false),
	ANSWERING_QUESTION(false, false),
	ENDED(false, false);
	
	GameStage(boolean answerable, boolean openForRegistration) {
		this.answerable = answerable;
		this.openForRegistration = openForRegistration;
	}
	
	private boolean answerable;
	private boolean openForRegistration;
	
	public boolean isAnswerable() {
		return answerable;
	}
	
	public boolean isOpenForRegistration() {
		return openForRegistration;
	}
}
