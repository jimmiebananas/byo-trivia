package com.mowbs.trivia.domain;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

public class GameState {
	
	private boolean started = false;
	private Map<String, Contestant> contestants = new ConcurrentHashMap<>();
	private Queue<Question> questions = new ConcurrentLinkedQueue<>();
	private Question currentQuestion;
	private String currentAnsweringContestant;
	private GameStage stage = GameStage.NOT_CREATED;
	private static String HOST;
	private Set<String> questionFilenames = new HashSet<>();
	private String currentQuestionFilename;
	private AudioState audioState;
	
	static {
		try {
			HOST = InetAddress.getLocalHost().getHostAddress();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getHost() {
		return HOST;
	}
	
	public boolean isStarted() {
		return started;
	}
	
	public boolean isOpenForRegistration() {
		return stage.isOpenForRegistration();
	}
	
	public void setStarted(boolean started) {
		this.started = started;
	}
	
	public Set<String> getQuestionFilenames() {
		return questionFilenames;
	}
	
	public void setQuestionFileNames(Set<String> questionFilenames) {
		this.questionFilenames = questionFilenames;
	}
	
	public String getCurrentQuestionFilename() {
		return currentQuestionFilename;
	}
	
	public void setCurrentQuestionFilename(String currentQuestionFilename) {
		if (this.questionFilenames.contains(currentQuestionFilename)) {
			this.currentQuestionFilename = currentQuestionFilename;
		}
	}
	
	public AudioState getAudioState() {
		return audioState;
	}
	
	public void setAudioState(AudioState audioState) {
		this.audioState = audioState;
	}
	
	public List<ContestantResponse> getContestants() {
		List<ContestantResponse> contestantsList = new ArrayList<>();
		
		contestantsList = contestants.values().stream().map(contestant -> {
			String id = contestant.getId();
			return new ContestantResponse(contestant, id.equals(currentAnsweringContestant));
		}).sorted().collect(Collectors.toList());
		
		return contestantsList;
	}
	
	public Contestant getContestantById(String contestantId) {
		Contestant foundContestant = null;
		if (contestants.containsKey(contestantId)) {
			foundContestant = contestants.get(contestantId);
		}
		return foundContestant;
	}

	public boolean isContestantNameAvailable(String name) {
		return contestants.values().stream().filter(contestant -> {
			return contestant.getName().equalsIgnoreCase(name);
		}).collect(Collectors.toList()).isEmpty();
	}

	public Contestant addContestant(String contestantName, PlayerSound buzzerSound) {
		String id = null;
		Contestant contestant = null;
		
		if (!contestants.containsValue(contestantName)) {
			id = UUID.randomUUID().toString();
			contestant = new Contestant(contestantName, id, buzzerSound);
			contestants.put(id, contestant);
		}
		
		return contestant;
	}
	
	public void setContestantScore(String contestantId, int score) {
		Contestant foundContestant = null;
		if (contestants.containsKey(contestantId)) {
			foundContestant = contestants.get(contestantId);
			foundContestant.setScore(score);
		}			
	}
	
	public void addQuestion(Question question) {
		this.questions.add(question);
	}
	
	public void addQuestions(List<Question> questions) {
		this.questions.addAll(questions);
	}
	
	public void clearQuestions() {
		this.questions.clear();
	}
	
	public void advanceToNextStageOfQuestion() {
		if (this.currentQuestion == null) {
			return;
		}

		this.currentQuestion.advanceToNextStage();
	}
	
	public Question advanceToNextQuestion() {
		if (this.questions.size() > 0) {
			this.currentQuestion = this.questions.poll();
			this.currentQuestion.revealAnswer();
			this.stage = GameStage.DISPLAYING_QUESTION;
			this.currentAnsweringContestant = null;
			this.contestants.values().forEach(contestant -> {
				contestant.setAnsweringEnabled(true);
			});
			return this.currentQuestion;
		} else {
			this.stage = GameStage.ENDED;
			return null;
		}

	}
	
	public void advanceToRevealAnswer() {
		if (this.stage == GameStage.ANSWERING_QUESTION || this.stage == GameStage.DISPLAYING_QUESTION) {
			this.stage = GameStage.DISPLAYING_ANSWER;
			this.currentQuestion.revealAnswer();
			this.currentQuestion.revealAllClues();
		}
	}
	
	public void advanceToNextTransition() {
		if (this.questions.size() > 0) {
			this.currentQuestion = this.questions.peek();
			this.stage = GameStage.DISPLAYING_TRANSITION;
			this.currentAnsweringContestant = null;
			this.contestants.values().forEach(contestant -> {
				contestant.setAnsweringEnabled(true);
			});
		} else {
			this.stage = GameStage.ENDED;
		}
	}
	
	public void incrementContestantScore(String contestantId, int incrementValue) {
		if (stage == GameStage.ANSWERING_QUESTION && currentAnsweringContestant != null) {
			getContestantById(contestantId).incrementScore(incrementValue);
		}
	}
	
	public void setAnsweringEnabled(String contestantId, boolean answeringEnabled) {
		getContestantById(contestantId).setAnsweringEnabled(answeringEnabled);
	}
	
	public Question getCurrentQuestion() {
		return currentQuestion;
	}
	
	public GameStage getStage() {
		return stage;
	}
	
	public void setStage(GameStage stage) {
		this.stage = stage;
	}
	
	public void setAnswering(String contestantId) {
		this.stage = GameStage.ANSWERING_QUESTION;
		this.currentAnsweringContestant = contestantId;
	}
	
	public String getAnsweringContestant() {
		return currentAnsweringContestant;
	}
	
	public void resetAnsweringContestant() {
		this.currentAnsweringContestant = null;
	}

}
