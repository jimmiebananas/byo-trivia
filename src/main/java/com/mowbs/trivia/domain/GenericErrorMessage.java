package com.mowbs.trivia.domain;

public class GenericErrorMessage {
	
	private String message;
	
	public GenericErrorMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

}
