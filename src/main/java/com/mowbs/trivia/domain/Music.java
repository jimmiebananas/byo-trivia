package com.mowbs.trivia.domain;

public enum Music implements Audio {
	
	INTRO_WAITING("pearly_whites_music"),
	CLUES("clues_music");
	
	Music(String fileName) {
		this.fileName = fileName;
	}
	
	private String fileName;
	
	@Override
	public String getFileName() {
		return fileName;
	}
}
