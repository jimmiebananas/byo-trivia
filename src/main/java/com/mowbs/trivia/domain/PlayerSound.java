package com.mowbs.trivia.domain;

public enum PlayerSound implements Audio {
	
	GAME_OVER("game_over"),
	BELCH("belch"),
	BOINGS("boings"),
	CARTOON_ARPEGGIO("cartoon_arpeggio"),
	DOG_SINGLE_BARK("dog_single_bark"),
	MONKEY_CHIMP("monkey_chimp"),
	PHOOOT("phooot"),
	PONY_WHINNY("pony_whinny"),
	SHEEP_BAH("sheep_bah"),
	SHOTGUN_COCK("shotgun_cock"),
	SINGLE_MOO("single_moo"),
	TELEPHONE_RINGER("telephone_ringer"),
	WHOOPEE_CUSION("whoopee_cushion"),
	RICHOCHET("ricochet");
	
	PlayerSound(String fileName) {
		this.fileName = fileName;
	}
	
	private String fileName;
	
	@Override
	public String getFileName() {
		return fileName;
	}
}