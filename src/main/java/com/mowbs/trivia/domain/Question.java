package com.mowbs.trivia.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Question {
	
	private int id;
	private QuestionType type;
	private String text;
	private Queue<String> stages = new ConcurrentLinkedQueue<>();
	private List<String> clues = new ArrayList<>();
	private String answer;
	private boolean showAnswer = false;
	
	public Question(QuestionType type, String text, int id, String answer) {
		this.type = type;
		this.text = text;
		this.id = id;
		this.answer = answer;
	}
	
	public int getId() {
		return id;
	}
	
	public String getText() {
		return text;
	}
	
	public QuestionType getType() {
		return type;
	}
	
	public void revealAnswer() {
		this.showAnswer = true;
	}
	
	public String getAnswer() {
		return this.showAnswer ? this.answer : null;
	}
	
	public enum QuestionType {
		STAGED
	}
	
	public List<String> getClues() {
		return clues;
	}
	
	public void revealAllClues() {
		while(this.stages.size() > 0) {
			this.clues.add(this.stages.poll());
		}
	}
	
	@JsonIgnore
	public Queue<String> getStages() {
		return stages;
	}
	
	public void setStages(Queue<String> stages) {
		this.stages = stages;
	}
	
	public void addStage(String stage) {
		this.stages.add(stage);
	}
	
	public void advanceToNextStage() {
		if (this.stages.size() > 0) {
			this.clues.add(this.stages.poll());
		}
	}
	
}
