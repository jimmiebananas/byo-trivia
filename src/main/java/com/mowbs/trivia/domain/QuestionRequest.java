package com.mowbs.trivia.domain;

import java.util.List;

public class QuestionRequest {
    
    private String answer;
    private List<String> clues;
    
    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    public List<String> getClues() {
        return clues;
    }
    public void setClues(List<String> clues) {
        this.clues = clues;
    }
}
