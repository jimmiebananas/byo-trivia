package com.mowbs.trivia.domain;

public class SetScoreRequest {
	private String contestantId;
	private int score;
	
	public String getContestantId() {
		return contestantId;
	}
	
	public void setContestantId(String contestantId) {
		this.contestantId = contestantId;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
}
