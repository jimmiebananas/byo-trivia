package com.mowbs.trivia.domain;

public class SocketDestinations {
	public static final String GAME_UPDATES = "/gameSocket";
	public static final String BUZZ_IN = "/buzzIn";
	public static final String UPDATE_GAME_STATE = "/update";
	public static final String CREATE_GAME = "/createGame";
	public static final String START_GAME = "/startGame";
	public static final String RESET_GAME = "/resetGame";
	public static final String CONTINUE = "/continueGame";
	public static final String ACCEPT_ANSWER = "/acceptAnswer";
	public static final String DECLINE_ANSWER = "/declineAnswer";
	public static final String NO_SCORE_ANSWER = "/noScoreAnswer";
	public static final String SET_CONTESTANT_SCORE = "/setScore";
	public static final String ENABLE_CONTESTANT = "/enableContestant";
	public static final String DISABLE_CONTESTANT = "/disableContestant";
	public static final String SET_MUSIC_VOLUME = "/setMusicVolume";
	public static final String SET_SFX_VOLUME = "/setSfxVolume";
}
