package com.mowbs.trivia.domain;

public enum SoundEffect implements Audio {
	
	START_GAME("record_scratch"),
	DECLINE_ANSWER("buzzer_console_error"),
	ACCEPT_ANSWER("applause_lighter"),
	CLUE_SHOWN("whoosh"),
	NO_SCORED("gentle_lost");
	
	SoundEffect(String fileName) {
		this.fileName = fileName;
	}
	
	private String fileName;
	
	@Override
	public String getFileName() {
		return fileName;
	}

}