package com.mowbs.trivia.service;

import java.io.BufferedInputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.FloatControl.Type;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.mowbs.trivia.domain.Audio;
import com.mowbs.trivia.domain.AudioState;
import com.mowbs.trivia.domain.AudioVolume;
import com.mowbs.trivia.domain.Music;

@Service
public class AudioManager {
	
	private static final String AUDIO_RESOURCE_PATH = "/audio/";
	private static final String AUDIO_FILE_EXT = ".wav";
	private static Clip currentMusic;
	private static final Object MUTEX = new Object();
	private static int currentMusicPosition = 0;
	private AudioVolume musicVolume = AudioVolume.SEVEN;
	private AudioVolume sfxVolume = AudioVolume.SEVEN;
	
	public AudioState getAudioState() {
		return new AudioState(musicVolume, sfxVolume);
	}
	
	public void playAudio(Audio audio) {
		try {
			ClassPathResource r = new ClassPathResource(AUDIO_RESOURCE_PATH + audio.getFileName() + AUDIO_FILE_EXT);
			AudioInputStream stream = AudioSystem.getAudioInputStream(new BufferedInputStream(r.getInputStream()));
			Clip clip = AudioSystem.getClip();
			clip.open(stream);
			adjustClipVolume(clip, sfxVolume);
			clip.start();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void adjustClipVolume(Clip clip, AudioVolume audioVolume) {
		 FloatControl gainControl = (FloatControl) clip.getControl(Type.MASTER_GAIN);
		 float decibalValue = audioVolume.getDecibalValue();
		 boolean isOverMax = decibalValue > gainControl.getMaximum();
		 boolean isUnderMin = decibalValue < gainControl.getMinimum();
		 if (!isOverMax && !isUnderMin) {
			 gainControl.setValue(audioVolume.getDecibalValue());
		 }
	}
	
	public AudioVolume getMusicVolume() {
		return musicVolume;
	}
	
	public AudioVolume getSfxVolume() {
		return sfxVolume;
	}
	
	public void setSfxVolume(AudioVolume audioVolume) {
		sfxVolume = audioVolume;
	}
	
	public void setMusicVolume(AudioVolume audioVolume) {
		musicVolume = audioVolume;
		
		try {
			if (currentMusic != null) {
				adjustClipVolume(currentMusic, audioVolume);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void startCurrentMusic(Music music) {
		synchronized(MUTEX) {
			if (currentMusic != null) {
				currentMusic.stop();
			}
			
			try {
				ClassPathResource r = new ClassPathResource(AUDIO_RESOURCE_PATH + music.getFileName() + AUDIO_FILE_EXT);
				AudioInputStream stream = AudioSystem.getAudioInputStream(new BufferedInputStream(r.getInputStream()));
				currentMusic = AudioSystem.getClip();
				currentMusic.open(stream);
				currentMusic.setFramePosition(0);
				adjustClipVolume(currentMusic, musicVolume);
				currentMusicPosition = 0;
				currentMusic.loop(Clip.LOOP_CONTINUOUSLY);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void stopCurrentMusic() {		
		synchronized(MUTEX) {
			if (currentMusic != null) {
				currentMusic.stop();
				currentMusicPosition = 0;
				currentMusic = null;
			}			
		}
	}
	
	public void pauseCurrentMusic() {
		synchronized(MUTEX) {
			if (currentMusic != null) {
				currentMusic.stop();
				currentMusicPosition = currentMusic.getFramePosition();
			}	
		}	
	}
	
	public void resumeCurrentMusic() {
		synchronized(MUTEX) {
			if (currentMusic != null && !currentMusic.isActive()) {
				currentMusic.setFramePosition(currentMusicPosition);
				adjustClipVolume(currentMusic, musicVolume);
				currentMusic.loop(Clip.LOOP_CONTINUOUSLY);
			}
		}
	}

}
