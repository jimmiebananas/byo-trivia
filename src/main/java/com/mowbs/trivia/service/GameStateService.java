package com.mowbs.trivia.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.mowbs.trivia.domain.Audio;
import com.mowbs.trivia.domain.AudioVolume;
import com.mowbs.trivia.domain.Contestant;
import com.mowbs.trivia.domain.GameStage;
import com.mowbs.trivia.domain.GameState;
import com.mowbs.trivia.domain.Music;
import com.mowbs.trivia.domain.PlayerSound;
import com.mowbs.trivia.domain.Question;
import com.mowbs.trivia.domain.SocketDestinations;
import com.mowbs.trivia.domain.SoundEffect;
import com.mowbs.trivia.web.exception.ContestantNameAlreadyTakenException;

@Service
public class GameStateService {
	
	private static final String DEFAULT_QUESTIONS_FILE = "dummyQuestions.csv";
	
	@Autowired
	private SimpMessagingTemplate messageTemplate;
	
	@Autowired
	private QuestionFileService questionFileService;
	
	@Autowired
	private AudioManager audioManager;
	
	private GameState gameState = new GameState();
	private Queue<PlayerSound> availableSounds = new ConcurrentLinkedQueue<>();
	
	@PostConstruct
	public void setup() {
		gameState.setAudioState(audioManager.getAudioState());
		loadQuestionFilenames();
		resetAvailableSounds();
	}
	
	private void loadQuestionFilenames() {
		synchronized(gameState) {
			gameState.setQuestionFileNames(questionFileService.getQuestionFilenames());
			gameState.setCurrentQuestionFilename(DEFAULT_QUESTIONS_FILE);
		}		
	}
	
	private void resetAvailableSounds() {
		this.availableSounds.clear();
		List<PlayerSound> soundList = Arrays.asList(PlayerSound.values());
		Collections.shuffle(soundList);
		availableSounds.addAll(soundList);
	}
	
	public void sendUpdate() {
		gameState.setAudioState(audioManager.getAudioState());
		messageTemplate.convertAndSend(SocketDestinations.UPDATE_GAME_STATE, gameState);
	}
	
	public void buzzIn(String contestantId) {
		
		synchronized(gameState) {
			if (gameState.getStage().isAnswerable()) {
				Audio buzzerSound = gameState.getContestantById(contestantId).getBuzzerSound();
				audioManager.pauseCurrentMusic();
				audioManager.playAudio(buzzerSound);
				gameState.setAnswering(contestantId);
			}
		}
		
		sendUpdate();
	}
	
	public GameState getGameState() {
		gameState.setAudioState(audioManager.getAudioState());
		return gameState;
	}
	
	public Contestant addContestant(String name) {
		Contestant contestant = null;
		
		synchronized(gameState) {
			if (gameState.isContestantNameAvailable(name)) {
				PlayerSound buzzerSound = availableSounds.poll();
				contestant = gameState.addContestant(name, buzzerSound);
				audioManager.playAudio(contestant.getBuzzerSound());
			} else {
				throw new ContestantNameAlreadyTakenException();
			}
		}
		
		sendUpdate();
		
		return contestant;
	}
	
	public void createGame() {
		synchronized(gameState) {
			gameState.setStage(GameStage.CREATED);
			audioManager.startCurrentMusic(Music.INTRO_WAITING);
		}
		
		sendUpdate();
		
	}
	
	public void startGame(String questionFilename) {
		if (questionFilename == null) {
			questionFilename = DEFAULT_QUESTIONS_FILE;
		}
		
		audioManager.stopCurrentMusic();
		if (audioManager.getMusicVolume() != AudioVolume.MUTE) {
			audioManager.playAudio(SoundEffect.START_GAME);
		}
		
		synchronized(gameState) {
			
			try {
				List<Question> questions = new ArrayList<>();
				questionFileService.reloadAll();
				questions = questionFileService.getQuestionsForFilename(questionFilename);
				gameState.clearQuestions();
				gameState.addQuestions(questions);
				gameState.setCurrentQuestionFilename(questionFilename);
				gameState.setQuestionFileNames(questionFileService.getQuestionFilenames());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			this.gameState.setStarted(true);
			this.gameState.setStage(GameStage.DISPLAYING_TRANSITION);

			sendUpdate();
		}
	}
	
	public void continueGame() {
		synchronized(gameState) {
			GameStage stage = gameState.getStage();
			if (stage == GameStage.DISPLAYING_ANSWER) {
				// Advance to the next transition
				this.gameState.advanceToNextTransition();
			} else if (stage == GameStage.DISPLAYING_TRANSITION) {
				// Advance to the next question
				audioManager.startCurrentMusic(Music.CLUES);
				this.gameState.advanceToNextQuestion();
			} else if (stage == GameStage.DISPLAYING_QUESTION) {				
				if (gameState.getCurrentQuestion().getStages().size() > 0) {
					// Advance to the next stage of the question
					audioManager.playAudio(SoundEffect.CLUE_SHOWN);
					this.gameState.advanceToNextStageOfQuestion();
				}
				// Otherwise, do nothing and just wait for accept/decline
			}
			
			sendUpdate();
		}
	}
	
	public void acceptAnswer(String contestantId) {
		synchronized(gameState) {
			audioManager.playAudio(SoundEffect.ACCEPT_ANSWER);
			this.gameState.incrementContestantScore(contestantId, 1);
			this.gameState.advanceToRevealAnswer();
			sendUpdate();
		}
	}
	
	public void declineAnswer(String contestantId) {
		synchronized(gameState) {
			audioManager.playAudio(SoundEffect.DECLINE_ANSWER);
			try {
				Thread.sleep(600);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			audioManager.resumeCurrentMusic();
			this.gameState.setStage(GameStage.DISPLAYING_QUESTION);
			this.gameState.setAnsweringEnabled(contestantId, false);
			this.gameState.resetAnsweringContestant();
			sendUpdate();
		}
	}
	
	public void setContestantScore(String contestantId, int score) {
		synchronized(gameState) {
			this.gameState.setContestantScore(contestantId, score);
		}
		
		sendUpdate();
	}
	
	public void disallowFurtherAnswering(String contestantId) {
		synchronized(gameState) {
			this.gameState.setAnsweringEnabled(contestantId, false);
		}
		
		sendUpdate();
	}
	
	public void reAllowFurtherAnswering(String contestantId) {
		synchronized(gameState) {
			this.gameState.setAnsweringEnabled(contestantId, true);
		}
		
		sendUpdate();
	}
	
	public void noScoreAnswer() {
		synchronized(gameState) {
			audioManager.stopCurrentMusic();
			audioManager.playAudio(SoundEffect.NO_SCORED);
			this.gameState.advanceToRevealAnswer();
			sendUpdate();
		}
	}
	
	public void reset(String questionFilename) {
		if (questionFilename == null) {
			questionFilename = DEFAULT_QUESTIONS_FILE;
		}
		synchronized(gameState) {
			try {
				audioManager.stopCurrentMusic();
				
				resetAvailableSounds();
				List<Question> questions = new ArrayList<>();
				questionFileService.reloadAll();
				questions = questionFileService.getQuestionsForFilename(questionFilename);
				gameState = new GameState();
				gameState.setAudioState(audioManager.getAudioState());
				gameState.addQuestions(questions);
				gameState.setCurrentQuestionFilename(questionFilename);
				gameState.setQuestionFileNames(questionFileService.getQuestionFilenames());
			} catch (Exception e) {
				e.printStackTrace();
			}
			sendUpdate();
		}	
	}
	
	public void setMusicVolume(AudioVolume musicVolume) {
		synchronized(gameState) {
			audioManager.setMusicVolume(musicVolume);
		}
		sendUpdate();
	}
	
	public void setSfxVolume(AudioVolume sfxVolume) {
		synchronized(gameState) {
			audioManager.setSfxVolume(sfxVolume);
		}
		sendUpdate();
	}

}
