package com.mowbs.trivia.service;

import java.io.File;
import java.io.FileFilter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.mowbs.trivia.domain.Question;
import com.mowbs.trivia.utils.QuestionFileLoader;

@Service
public class QuestionFileService {
	
	private Map<String, List<Question>> questionsToFilenameMap = new ConcurrentHashMap<>();
	private static final String PWD = "user.dir";
	private static final String QUESTION_FILES_FOLDER_NAME = "questionFiles";
	
	public void reloadAll() {
		questionsToFilenameMap.clear();
		findQuestionFiles();
	}
	
	public List<Question> getQuestionsForFilename(String filename) {
		return questionsToFilenameMap.get(filename);
	}
	
	public Set<String> getQuestionFilenames() {
		return questionsToFilenameMap.keySet();
	}
	
	@PostConstruct
	public void findQuestionFiles() {
		String currentDirectory = System.getProperty(PWD);
		String questionsFileFolderPath = currentDirectory + File.separator + QUESTION_FILES_FOLDER_NAME;
		File questionFilesFolder = new File(questionsFileFolderPath);
		
		if (questionFilesFolder.exists() && questionFilesFolder.isDirectory()) {
			File[] csvQuestionFiles = questionFilesFolder.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathName) {
					return pathName.getName().endsWith(".csv");
				}
			});
			
			for (File questionFile : csvQuestionFiles) {
				String filename = questionFile.getName();
				QuestionFileLoader fileLoader = new QuestionFileLoader(filename);
				List<Question> loadedQuestions = fileLoader.loadQuestions();

				try {
					questionsToFilenameMap.put(filename, loadedQuestions);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
		 
	}

}
