package com.mowbs.trivia.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import com.mowbs.trivia.domain.Question;
import com.mowbs.trivia.domain.Question.QuestionType;
import com.mowbs.trivia.domain.QuestionRequest;

public class QuestionFileLoader {
	
	private final String fileName;
	private static final String PWD = "user.dir";
	private static final String QUESTIONS_FOLDER = "questionFiles";
	public static final String QUESTIONS_FOLDER_ABS_PATH = System.getProperty(PWD) + File.separator + QUESTIONS_FOLDER;
	private CSVFormat csvFormat = CSVFormat.DEFAULT.withHeader();
    private static final Object [] FILE_HEADER = { "answer", "clue1", "clue2", "clue3", "clue4", "clue5", "clue6", "clue7" };
	
	public QuestionFileLoader(String fileName) {
		this.fileName = fileName;
	}
	
	public void writeQuestions(List<QuestionRequest> questions) throws IOException {
	    File cluesDirectory = new File(QUESTIONS_FOLDER_ABS_PATH);
	    File questionsFile = new File(cluesDirectory.getAbsolutePath() + File.separator + this.fileName);
	    
	    FileWriter fileWriter = new FileWriter(questionsFile);
	    CSVPrinter printer = new CSVPrinter(fileWriter, CSVFormat.DEFAULT);
	    printer.printRecord(FILE_HEADER);
	    
	    try {
	        questions.stream().map(this::createCsvRecordFromQuestion).forEach(record -> {
	            try {
                    printer.printRecord(record);
                } catch (Exception e) {
                    e.printStackTrace();
                }
	        });
	    } finally {
	        printer.flush();
	        IOUtils.closeQuietly(printer);
	    }
	}
	
	public List<String> createCsvRecordFromQuestion(QuestionRequest question) {
	    List<String> record = new ArrayList<>();
	    record.add(question.getAnswer());
	    record.addAll(question.getClues());
	    return record;
	}
	
	public Question createQuestionFromCsvRecord(CSVRecord record, Set<String> headers, int questionNumber) {
        Question question = new Question(QuestionType.STAGED, null, questionNumber, record.get("answer"));
        
        headers.forEach(header -> {
            if (!header.equalsIgnoreCase("answer")) {
                question.addStage(record.get(header));
            }            
        });

        return question;
	}
	
	public List<Question> loadQuestions() {
		File cluesDirectory = new File(QUESTIONS_FOLDER_ABS_PATH);
		File questionsFile = new File(cluesDirectory.getAbsolutePath() + File.separator + this.fileName);
		System.out.println("************************** Looking for questions file: " + questionsFile.getAbsolutePath() + " **************************");
		
		List<Question> questionsList = new ArrayList<>();
		
		try {
			Reader input = new FileReader(questionsFile);
			CSVParser parser = csvFormat.parse(input);
	        Set<String> headers = parser.getHeaderMap().keySet();
			int questionNumber = 0;
			
			for (CSVRecord record : parser) {
				questionNumber++;
				Question question = createQuestionFromCsvRecord(record, headers, questionNumber);
				questionsList.add(question);
			}
	
			System.out.println("************************** Loaded " + questionsList.size() + " questions from " + questionsFile.getAbsolutePath() + " **************************");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return questionsList;
	}	
}
