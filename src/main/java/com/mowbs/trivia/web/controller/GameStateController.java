package com.mowbs.trivia.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mowbs.trivia.domain.AdjustVolumeRequest;
import com.mowbs.trivia.domain.Contestant;
import com.mowbs.trivia.domain.GameState;
import com.mowbs.trivia.domain.SetScoreRequest;
import com.mowbs.trivia.domain.SocketDestinations;
import com.mowbs.trivia.service.GameStateService;

@RestController
public class GameStateController {
	
	@Autowired
	private GameStateService gameStateService;
	
	@RequestMapping(value = "/gameState", method = RequestMethod.GET)
	@ResponseBody
	public GameState getGameState(HttpServletRequest request) throws Exception {		
		return gameStateService.getGameState();
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	public Contestant registerForGame(@RequestBody Contestant contestant) {
		return gameStateService.addContestant(contestant.getName().trim());
	}
	
	@MessageMapping(SocketDestinations.CREATE_GAME)
	public void createGame() {
		gameStateService.createGame();
	}
	
	@MessageMapping(SocketDestinations.START_GAME)
	public void startGame(String questionFilename) {
		gameStateService.startGame(questionFilename);
	}
	
	@MessageMapping(SocketDestinations.RESET_GAME)
	public void resetGame(String questionFilename) {
		gameStateService.reset(questionFilename);
	}
	
	@MessageMapping(SocketDestinations.BUZZ_IN)
	public void handleBuzzIn(String contestantId) throws Exception {
		gameStateService.buzzIn(contestantId);
	}
	
	@MessageMapping(SocketDestinations.CONTINUE)
	public void handleContinue() {
		gameStateService.continueGame();
	}
	
	@MessageMapping(SocketDestinations.ACCEPT_ANSWER)
	public void handleAcceptAnswer(String contestantId) {
		gameStateService.acceptAnswer(contestantId);
	}
	
	@MessageMapping(SocketDestinations.DECLINE_ANSWER)
	public void handleDeclineAnswer(String contestantId) {
		gameStateService.declineAnswer(contestantId);
	}
	
	@MessageMapping(SocketDestinations.NO_SCORE_ANSWER)
	public void handleNoScoreAnswer() {
		gameStateService.noScoreAnswer();
	}
	
	@MessageMapping(SocketDestinations.SET_CONTESTANT_SCORE)
	public void handleIncrementContestantScore(SetScoreRequest setScoreRequest) {
		gameStateService.setContestantScore(setScoreRequest.getContestantId(), setScoreRequest.getScore());
	}
	
	@MessageMapping(SocketDestinations.ENABLE_CONTESTANT)
	public void handleEnableContestant(String contestantId) {
		gameStateService.reAllowFurtherAnswering(contestantId);
	}
	
	@MessageMapping(SocketDestinations.DISABLE_CONTESTANT)
	public void handleDisableContestant(String contestantId) {
		gameStateService.disallowFurtherAnswering(contestantId);
	}
	
	@MessageMapping(SocketDestinations.SET_MUSIC_VOLUME)
	public void handleSetMusicVolume(AdjustVolumeRequest volumeRequest) {
		gameStateService.setMusicVolume(volumeRequest.getVolume());
	}
	
	@MessageMapping(SocketDestinations.SET_SFX_VOLUME)
	public void handleSetSfxVolume(AdjustVolumeRequest volumeRequest) {
		gameStateService.setSfxVolume(volumeRequest.getVolume());
	}
}
