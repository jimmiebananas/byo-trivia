package com.mowbs.trivia.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mowbs.trivia.domain.QuestionRequest;
import com.mowbs.trivia.service.QuestionFileService;
import com.mowbs.trivia.utils.QuestionFileLoader;

@RestController
public class QuestionsController {

    @Autowired
    private QuestionFileService questionFileService;
    
    @RequestMapping(value = "/questionFiles/{questionFileName}", method = RequestMethod.GET)
    @ResponseBody
    public List<QuestionRequest> getQuestionsFile(@PathVariable String questionFileName) {
        String fileNameWithExtension = questionFileName + ".csv";
        return questionFileService.getQuestionsForFilename(fileNameWithExtension).stream().map(question -> {
            QuestionRequest request = new QuestionRequest();
            question.revealAllClues();
            question.revealAnswer();
            request.setAnswer(question.getAnswer());
            request.setClues(question.getClues());
            return request;
        }).collect(Collectors.toList());
    }
    
    @RequestMapping(value = "/questionFiles/{questionFileName}", method = RequestMethod.PUT)
    @ResponseBody
    public List<QuestionRequest> saveQuestionsFile(@RequestBody List<QuestionRequest> questions, @PathVariable String questionFileName) throws Exception {
        QuestionFileLoader loader = new QuestionFileLoader(questionFileName.trim().toLowerCase() + ".csv");
        loader.writeQuestions(questions);
        questionFileService.reloadAll();
        return questions;
    }
}
