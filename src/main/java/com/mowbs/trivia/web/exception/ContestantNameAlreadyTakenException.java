package com.mowbs.trivia.web.exception;

public class ContestantNameAlreadyTakenException extends RuntimeException {

	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = 8680605049636578448L;
	private static final String MSG = "That name is already taken";
	
	public ContestantNameAlreadyTakenException() {
		super(MSG);
	}

}
