package com.mowbs.trivia.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.mowbs.trivia.domain.GenericErrorMessage;

@ControllerAdvice
public class ExceptionHandlingController {

	@ExceptionHandler(ContestantNameAlreadyTakenException.class)
	public ResponseEntity<GenericErrorMessage> handleContestantNameTaken(ContestantNameAlreadyTakenException exception) {
		return new ResponseEntity<>(new GenericErrorMessage(exception.getMessage()), HttpStatus.BAD_REQUEST);
	}
}
